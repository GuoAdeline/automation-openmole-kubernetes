# Automation
## Deployment Kubernetes 
### Method 1 : Create OpenStack instances and deploy a kubernetes cluster 

* Openstack credentials
  ```sh
  git clone https://gitlab.openmole.org/openmole/automation-openmole-kubernetes.git
  cd automation-openmole-kubernetes/deployment_kubernetes/
  source openmole-test-openrc.sh
  vim main.tf
  ```
  change `YOUR_OPENSTACK_USERNAME` and `YOUR_OPENSTACK_PASSWORD` to your openstack username and password in block `variable "username"` and `variable "password"`
  
* Deployment Kubernetes
  ```sh
  bash terraform.sh
  ```
  After about 20 min, you will see the following information, which signifies that a kubernetes cluster is ready in OpenStack :
  ```
  Apply complete! Resources: 4 added, 0 changed, 0 destroyed.

  Outputs:

  cluster-manager = $ ssh ubuntu@134.158.75.106
  kubernetes-node = $ ssh ubuntu@134.158.74.115 / $ ssh ubuntu@134.158.74.241
  ```
  View more details [here](/deployment_kubernetes)
### Method 2 : On physical machines :
* Prerequisites
  * 3 node machines Linux with known IP addresses and  `python-pip` installed.
  
    You can install `python-pip` with :
    ```sh
    sudo apt-get update
    sudo apt install python-pip
    ```
  * ```sh
    vim ./deployment_kubernetes/kube_physique_machine.sh
    ```
    change `$YOUR_USERNAME` to your machine username, change `$IP1`, `$IP2` and `$IP3` to IP addresses of your node machines. 
    
    You can add as many machines as you want by adding ip address in `IPLIST`
* Deployment kubernetes
  ```sh
  cd deployment_kubernetes/
  bash ./kube_physique_machine.sh
  ```

## Deployment OpenMOLE in cluster kubernetes
* Prerequisite
  * Kubernetes cluster deployed.  
  * Docker image for `openmole-connect`. Here we use image `mm768528/openmole-connect:latest` on Docker Hub. View [here](https://gitlab.com/GuoAdeline/openmole-kubernetes/tree/master/docker/make_docker_image_for_openmole) to see how to make docker image. You can use your own image by changing it in the ninth line of file `/deployment_openmole/openmole.sh`
  * Public hostname associated to one of your machines. Replace the hostname `intern.iscpif.fr` in the file `/deployment_openmole/connect_ing.yaml` with your own hostname.
  * Disable Swap on Kubernetes Node : `sudo swapoff -a`, otherwise Kubernetes will not work properly.
* Connect to one of the node machines, for exemple:
  ```sh
  ssh ubuntu@134.158.74.115
  ```
  **Attention :** In `deployment_openmole/config` file, we defined an admin user `kubernetes-admin`. 
* Deployment in node machine :
  ```sh
  git clone https://gitlab.openmole.org/openmole/automation-openmole-kubernetes
  cd automation-openmole-kubernetes/deployment_openmole 
  bash openmole.sh   # comment out the line 'cp config ~/.kube/config' in file openmole.sh if you are testing locally 
  ```
  You will deploy a ingress-nginx, openmole-connect and two openmole instances for user "foo" and "bar" in the cluster.
  
  To add instance for new user, add the following command in file `deployment_openmole/openmole.sh`, change `$uuid` to your user uuid:
  ```sh
  kubectl run openmole-$uuid --image=openmole/openmole -n ingress-nginx -- openmole --port 80 --password password --remote --http
  kubectl expose deployment openmole-$uuid --port=80 -n ingress-nginx
  ```
* Check if deployment success :
  ```
  kubectl get po,deploy,svc,ing --all-namespaces
  ```
* Problem that you mignt meet when you do local test : 
  * Can't deploy instance in the cluster, run the command by replacing `$YOUR_NODE_NAME` by your master node name :
    ```sh
    kubectl get nodes
    kubectl taint node $YOUR_NODE_NAME node-role.kubernetes.io/master-
    ```
    Then you can do the deployment in cluster.
  * No address for ingress, run the command by replacing `$YOUR_IP_ADDRESS` by your ip address :
    ```
    kubectl patch svc ingress-nginx -n ingress-nginx -p '{"spec": {"type": "LoadBalancer", "externalIPs":["YOUR_IP_ADDRESS"]}}' 
    ```
    
### View [here](https://gitlab.openmole.org/openmole/openmole-kubernetes/tree/master/kubernetes/kubernetes%20handbook) for some useful kubernetes commands.

### View [here](https://gitlab.openmole.org/openmole/openmole-kubernetes/tree/master/kubernetes/installation/Installation_manual) for local manual installation of Kubernetes. 

### Please refer [here](https://gitlab.openmole.org/openmole/openmole-kubernetes) for more details.
