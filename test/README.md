# Test with openmole-connect
## After you have changed something in openmole-connect, rebuild the docker image :
```
cd openmole-connect/
sbt application/docker:publishLocal
docker images
docker tag <IMAGE ID> <yourhubusername>/<IMAGE NAME>:latest
docker push <yourhubusername>/<REPOSITORY NAME>
```
for exemple :
```
docker tag 619ff956bec9 mm768528/openmole-connect:latest
docker push mm768528/openmole-connect
```

If you do a test local, no need to run the last two lines. 

## Recreate deployment openmole-connect in kubernetes : 
Replace `$YOUR_IMAGE_OPENMOLE_CONNECT` bt your openmole-connect image
```
kubectl delete deployment openmole-connect --namespace=ingress-nginx
kubectl delete svc openmole-connect --namespace=ingress-nginx
kubectl run openmole-connect --image=$YOUR_IMAGE_OPENMOLE_CONNECT --replicas=1 --namespace=ingress-nginx -- --secret 6e824d88-b17e-4534-8ed6-f69ba5f29845 --public-adress http://intern.iscpif.fr
kubectl expose deployment openmole-connect --port=8080 --namespace=ingress-nginx
```

## Problem you might meet
**Error of ImagePullBackOff**
```
kubectl edit deployment opnemole-connect -n ingress-nginx
```
change `imagePullPolicy: Always` to `imagePullPolicy: IfNotPresent`