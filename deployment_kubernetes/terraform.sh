#!/usr/bin/env bash
wget https://releases.hashicorp.com/terraform/0.12.8/terraform_0.12.8_linux_amd64.zip
sudo apt install unzip
unzip terraform_0.12.8_linux_amd64.zip
sudo mv terraform /usr/local/bin/
rm terraform_0.12.8_linux_amd64.zip

FILE=~/.ssh/id_rsa
if [ -f "$FILE" ]; then
    echo "Use existing $FILE file."
else
    ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
fi

terraform init -input=false
terraform plan -out=tfplan -input=false
terraform apply -input=false tfplan
