provider "openstack" {
        auth_url = "https://keystone.lal.in2p3.fr:5000/v3"
        region = "lal"
        insecure = "true"
        user_domain_name = "stratuslab"
        project_domain_id = "6245e15fd3fc4faf9a5b8a2805f926eb"
}

variable "username" {
  description = "Your OpenStack username"
  default = "YOUR_OPENSTACK_USERNAME"
}
variable "password" {
  description = "Your OpenStack password"
  default = "YOUR_OPENSTACK_PASSWORD"
}
variable "compute_count" {
    default = 2
}
variable "cluster_name" {
    default = "testing"
}
variable "network_name" {
    default = "public"
}
variable "floatingip_pool" {
    default = "public"
}
variable "cluster_image" {
    default = "ubuntu-16.04-20160905"
}
variable "cluster_flavor" {
    default = "os.1"
}
variable "tenant" {
  description = "openmole-test"
  default = "openmole-test"
}
variable "public_key_path" {
  description = "The path of the ssh pub key"
  default = "~/.ssh/id_rsa.pub"
}
variable "keypair" {
  description = "Your Key pair name"
  default = "mykey"
}

#Comment out this block if you are not running for the first time
resource "openstack_compute_keypair_v2" "key_pair" {
  name = "${var.keypair}"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}


resource "openstack_compute_instance_v2" "controller" {
  name            = "central-manager"
  image_name      = "${var.cluster_image}"
  flavor_name     = "${var.cluster_flavor}"
  key_pair        = "${var.keypair}"
  security_groups = ["default"]

  network {
    name = "public"
  }

  provisioner "remote-exec" {
	inline = [           
           "#!/usr/bin/env bash",
           "sudo apt-get install sshpass",
           "ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''",
           "sshpass ssh-copy-id -o StrictHostKeyChecking=no ubuntu@${openstack_compute_instance_v2.compute.0.access_ip_v4}",
           "sshpass ssh-copy-id -o StrictHostKeyChecking=no ubuntu@${openstack_compute_instance_v2.compute.1.access_ip_v4}",
           "sudo apt-get update",
           "sudo apt --assume-yes install python-pip",
           "sudo apt-get update",
           "sudo apt-get --assume-yes install software-properties-common",
           "sudo apt-add-repository -y ppa:ansible/ansible",
           "sudo apt-get update",
           "sudo apt-get --assume-yes install ansible python-netaddr python-pbr",
           "sudo apt-get install unzip",
           "wget https://releases.hashicorp.com/terraform/0.12.8/terraform_0.12.8_linux_amd64.zip",
           "unzip terraform_0.12.8_linux_amd64.zip",
           "rm terraform_0.12.8_linux_amd64.zip",
           "sudo mv terraform /usr/local/bin/",
           "pip install jinja2",
           "pip2 install jinja2 --upgr",
           "wget https://github.com/kubernetes-incubator/kubespray/archive/v2.1.2.tar.gz",
           "tar -zxvf v2.1.2.tar.gz",
           "rm v2.1.2.tar.gz",
           "mv kubespray-2.1.2 kubespray",
           "cd kubespray",
           "pip install -r requirements.txt",
           "IP=(${openstack_compute_instance_v2.compute.0.access_ip_v4} ${openstack_compute_instance_v2.compute.1.access_ip_v4})",
           "CONFIG_FILE=./inventory/inventory.cfg python3 ./contrib/inventory_builder/inventory.py $${IP[*]}",
           "ansible-playbook -i inventory/inventory.cfg cluster.yml -b -v --private-key=~/.ssh/id_rsa",
	]
        connection {
         host = "${self.access_ip_v4}"
         user = "ubuntu"
         type = "ssh"
         private_key = "${file("~/.ssh/id_rsa")}"
        }
  }
  depends_on = [
        "openstack_compute_instance_v2.compute"
  ]

}

resource "openstack_compute_instance_v2" "compute" {
  name            = "compute${count.index}"
  count           = "${var.compute_count}"
  image_name      = "${var.cluster_image}"
  flavor_name     = "${var.cluster_flavor}"
  key_pair        = "${var.keypair}"
  security_groups = ["default"]
  
  network {
    name = "public"
  }
  
  provisioner "remote-exec" {
        inline = [
           "sudo apt-get update",
           "sudo apt --assume-yes install python-pip",
        ]
        connection {
         host = "${self.access_ip_v4}"
         user = "ubuntu"
         type = "ssh"
         private_key = "${file("~/.ssh/id_rsa")}"
        }
  }

}

output "cluster-manager" {
    value = "$ ssh ubuntu@${openstack_compute_instance_v2.controller.access_ip_v4}"
}

output "kubernetes-node" {
    value = "$ ssh ubuntu@${openstack_compute_instance_v2.compute.0.access_ip_v4} / $ ssh ubuntu@${openstack_compute_instance_v2.compute.1.access_ip_v4}"
}

