#!/usr/bin/env bash
USERNAME=$YOUR_USERNAME
IPLIST="$IP1 $IP2 $IP3"
sudo apt-get install sshpass
FILE=~/.ssh/id_rsa
if [ -f "$FILE" ]; then
    echo "Use existing $FILE file."
else
    ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
fi
for ip in $IPLIST
do
sshpass ssh-copy-id -o StrictHostKeyChecking=no $USERNAME@$ip
done
sudo apt-get update
sudo apt --assume-yes install python-pip
sudo apt-get update
sudo apt-get --assume-yes install software-properties-common
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
sudo apt-get --assume-yes install ansible python-netaddr python-pbr
sudo apt-get install unzip
wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.12.8_linux_amd64.zip
unzip terraform_0.12.8_linux_amd64.zip
rm terraform_0.12.8_linux_amd64.zip
sudo mv terraform /usr/local/bin/
pip install jinja2
pip2 install jinja2 --upgr
wget https://github.com/kubernetes-incubator/kubespray/archive/v2.1.2.tar.gz
tar -zxvf v2.1.2.tar.gz
rm v2.1.2.tar.gz
mv kubespray-2.1.2 kubespray
cd kubespray
pip install -r requirements.txt
IP=$IPLIST
CONFIG_FILE=./inventory/inventory.cfg python3 ./contrib/inventory_builder/inventory.py ${IP[*]}
ansible-playbook -i inventory/inventory.cfg cluster.yml -b -v --private-key=~/.ssh/id_rsa

