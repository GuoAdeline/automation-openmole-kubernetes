# Tell Skuber to configure itself from the default Kubeconfig file ($HOME/.kube/config):
#cp config ~/.kube/config
export SKUBER_CONFIG=file

# Deploy ingress-nginx
kubectl apply -f ingress_nginx.yaml

# Deploy openmole-connect
kubectl run openmole-connect --image=mm768528/openmole-connect:latest --replicas=1 --namespace=ingress-nginx -- --secret 6e824d88-b17e-4534-8ed6-f69ba5f29845 --public-adress http://intern.iscpif.fr
kubectl expose deployment openmole-connect --port=8080 --namespace=ingress-nginx

# Expose openmole-connect with ingress
kubectl apply -f connect_ing.yaml

# Deploy openmole instance. We create here two instances openmole for user "foo" and "bar"
kubectl run openmole-foo-123-567-foo --image=openmole/openmole -n ingress-nginx -- openmole --port 80 --password password --remote --http
kubectl expose deployment openmole-foo-123-567-foo --port=80 -n ingress-nginx

kubectl run openmole-bar-123-567-bar --image=openmole/openmole -n ingress-nginx -- openmole --port 80 --password password --remote --http
kubectl expose deployment openmole-bar-123-567-bar --port=80 -n ingress-nginx

# Bind "cluster-admin" role to openmole-connect so that it can access to other pods in the cluster
kubectl apply -f roleBinding.yaml
